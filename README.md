# btcpay-test

Here are the steps you can take to try out my code and create a Bitcoin invoice with an http request.

First go to https://btcpayjungle.com and create an account.

Next create a store and (perhaps optionally) connect a wallet to the store.
For more detailed instructions on how to set up the store, see [this](https://medium.com/@caribbeanblockchain/btcpay-implementation-guide-accept-crypto-as-a-payment-method-in-the-caribbean-eef3ae1f0ce5) starting from the "Setting up BTCPay Account" section.

Next, clone this repository and in the root of repo, generate a compressed private key with openssl:
```
openssl ecparam -genkey -name secp256k1 -rand /dev/urandom -out private-key.pem
openssl ec -in private-key.pem -outform DER|tail -c +8|head -c 32|xxd -p -c 32 > private-key-compressed
```
As specified [in this question](https://bitcoin.stackexchange.com/questions/59644/how-do-these-openssl-commands-create-a-bitcoin-private-key-from-a-ecdsa-keypair).

Before you run the haskell code, make sure you have the runtime dependency: the secp256k1 C library.
This can be installed on Ubuntu 18.04 with:
```
sudo apt install libsecp256k1-dev
```

Next, run `stack build && stack exec btcpay-test`
And follow the instructions to approve client pairing and create an invoice.
