{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Crypto.Secp256k1 (PubKey, SecKey, derivePubKey, exportPubKey, exportSig, msg, signMsg, secKey)
import qualified Data.Aeson as JSON
import qualified Data.Aeson.Types as JSON
import qualified Data.ByteString as B
import qualified Data.ByteString.Base16 as B16
import Data.ByteString.Lazy (toStrict)
import qualified Data.ByteString.UTF8 as UTF8
import Data.Function ((&))
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Monoid ((<>))
import qualified Data.Serialize as C
import GHC.Generics (Generic)
import Network.Haskoin.Crypto (addressHash, sha256)
import Network.Haskoin.Address.Base58 (Base58, encodeBase58Check)
import Network.HTTP.Simple (Request, addRequestHeader, getResponseBody, httpJSON, parseRequest_, setRequestBodyJSON)
import Network.HTTP.Types.Header (hContentType)

getSecretKey :: IO SecKey
getSecretKey = decodeSecretKeyOrFail . fst . B16.decode <$> B.readFile "private-key-compressed"
  where
    decodeSecretKeyOrFail :: B.ByteString -> SecKey
    decodeSecretKeyOrFail = fromMaybe err . secKey
    err = error $
          "The file `private-key-compressed` does not contain a properly encoded private key.\n"
       <> "It should contain a 32 byte compressed ECDSA private key.\n"
       <> "Please see: https://bitcoin.stackexchange.com/questions/59644/how-do-these-openssl-commands-create-a-bitcoin-private-key-from-a-ecdsa-keypair\n"
       <> "for instructions on generating your private key."

printPublicKey :: IO ()
printPublicKey = B16.encode . exportPubKey True . derivePubKey <$> getSecretKey >>= print

-- Calculate a SIN as described here: https://en.bitcoin.it/wiki/Identity_protocol_v1
makeSIN :: PubKey -> Base58
makeSIN = encodeBase58Check . fst . B16.decode . ("0F02" <>) . B16.encode . C.encode . addressHash . exportPubKey True

type URL = String

signRequest :: JSON.ToJSON body => SecKey -> URL -> body -> Request
signRequest key url body = parseRequest_ ("POST " <> url)
    & addRequestHeader "x-identity" pubKey
    & addRequestHeader "x-signature" signature
    & addRequestHeader hContentType "application/json"
    & addRequestHeader "x-accept-version" "2.0.0"
    & setRequestBodyJSON body
  where
    message = fromMaybe (error "invalid message") . msg . C.encode . sha256 $ UTF8.fromString url <> toStrict (JSON.encode body)
    signature = B16.encode . exportSig $ signMsg key message
    pubKey = B16.encode . exportPubKey True $ derivePubKey key

data TokenRequest = TokenRequest
  { label :: String
  , id :: Base58
  , facade :: String
  } deriving (Generic)

instance JSON.ToJSON TokenRequest

type PairingCode = String

data TokenResponse = TokenResponse
  { pairingCode :: PairingCode
  , token :: String
  } deriving (Generic, Show)

instance JSON.FromJSON TokenResponse

pairingURL :: PairingCode -> URL
pairingURL pairingCode = "https://btcpayjungle.com/api-access-request?pairingCode=" <> pairingCode

printTokenURL :: PairingCode -> IO ()
printTokenURL = putStrLn . pairingURL

data InvoiceRequest = InvoiceRequest
  { price :: String
  , currency :: String
  , token :: String
  } deriving (Generic)

instance JSON.ToJSON InvoiceRequest

data InvoiceResponse = InvoiceResponse
  { id :: String
  , btcDue :: String
  , url :: String
  , bitcoinAddress :: String
  } deriving (Generic, Show)

instance JSON.FromJSON InvoiceResponse

main :: IO ()
main = do
  secretKey <- getSecretKey
  let label = "BTCPay Test Client 14"
      id = makeSIN $ derivePubKey secretKey
      facade = "merchant"
      body = TokenRequest label id facade
      url = "https://btcpayjungle.com/tokens"
      request = parseRequest_ ("POST " <> url) & setRequestBodyJSON body
  responseBody <- getResponseBody <$> httpJSON request :: IO JSON.Value
  let pairingResponse :: TokenResponse
      pairingResponse = fromMaybe (error "could not parse TokenResponse") $ listToMaybe =<< JSON.parseMaybe (JSON.withObject "pairingDatas" (JSON..: "data")) responseBody
  putStrLn "Please go to:"
  printTokenURL $ pairingCode pairingResponse
  putStrLn "And approve pairing this client."
  putStrLn "Then press enter to create an invoice."
  _ <- getChar
  invoiceBody <- fmap getResponseBody . httpJSON . signRequest secretKey "https://btcpayjungle.com/invoices" . InvoiceRequest "114" "USD" $ token (pairingResponse :: TokenResponse)
  let invoiceResponse :: InvoiceResponse
      invoiceResponse = either error Prelude.id $ JSON.parseEither (JSON.withObject "invoiceData" (JSON..: "data")) invoiceBody
  print invoiceResponse
